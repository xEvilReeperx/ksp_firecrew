﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using ReeperCommon;
using UnityEngine;

namespace FireCrew.Logic
{
    /// <summary>
    /// In the MultiButton setting, the ButtonListener is effectly disabled and automatic
    /// highlighting is lost. We don't want to just re-enable it as the ButtonListener
    /// accomplishes this by switching between two GameObjects, the second of which is
    /// slightly less wide to accomodate a small button.
    /// 
    /// We can fake the effect by messing with the renderer material brightness though
    /// </summary>
    class HighlightOnMouseover : MonoBehaviour
    {
        private readonly Color DarkColor = new Color(0.5f, 0.5f, 0.5f);
        private readonly Color LightColor = new Color(0.7f, 0.7f, 0.7f);

        Material material;

        private void Start()
        {
            var bg = transform.Find("bg");

            if (bg == null)
            {
                Log.Error("HighlightOnMouseover.Start: failed to find bg");
                Component.Destroy(this);
            }
            else
            {
                material = bg.renderer.material;

                bg.GetComponent<BTButton>().SetInputDelegate(delegate(ref POINTER_INFO ptr)
                {
                    switch (ptr.evt)
                    {
                        case POINTER_INFO.INPUT_EVENT.MOVE_OFF:
                            material.color = DarkColor;
                            break;

                        case POINTER_INFO.INPUT_EVENT.MOVE:
                            material.color = LightColor;
                            break;
                    }
                });
            }
        }
    }
}
