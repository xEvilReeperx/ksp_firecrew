﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using System.Collections.Generic;
using System.Linq;
using ReeperCommon;
using UnityEngine;

namespace FireCrew.Logic
{
    using DismissStyles;

    /// <summary>
    /// Adds all three dismissal options to each list entry: dismissal, retraining and retirement
    /// They're added as child buttons
    /// </summary>
    class MultiButton : MonoBehaviour
    {
        private const float SliderAdjustmentMultiplier = 0.4f; // new position of the sliders as a fraction of the total width of the list item container
        private const float BUTTON_WIDTH = 100f;
        private const float BUTTON_HEIGHT = 16f;

        FireKerbal fire = new FireKerbal();
        RetireKerbal retire = new RetireKerbal();
        RetrainKerbal retrain = new RetrainKerbal();

        /******************************************************************************
         *                    Implementation Details
         ******************************************************************************/

        private void Start()
        {
            Log.Debug("MultiButton.Start");

            GetComponent<ButtonListener>().Button = ButtonListener.ButtonType.None;

            Material material = new Material(Shader.Find("Sprite/Vertex Colored")) { mainTexture = ResourceUtil.GetEmbeddedTexture("FireCrew.Resources.ButtonSheet.png") };
            UIButton fireButton = CreateButton("MultiButton.Fire", material);
            UIButton retrainButton = CreateButton("MultiButton.Retrain", material);
            UIButton retireButton = CreateButton("MultiButton.Retire", material);

            UVAnimation normal = new UVAnimation();

            fireButton.animations[0].SetAnim(normal.BuildUVAnim(new Vector2(0f, BUTTON_HEIGHT).PixelCoordToUVCoord(material.mainTexture), new Vector2(BUTTON_WIDTH, BUTTON_HEIGHT).PixelSpaceToUVSpace(material.mainTexture), 1, 1, 1));
            fireButton.animations[1].SetAnim(normal.BuildUVAnim(new Vector2(BUTTON_WIDTH, BUTTON_HEIGHT).PixelCoordToUVCoord(material.mainTexture), new Vector2(BUTTON_WIDTH, BUTTON_HEIGHT).PixelSpaceToUVSpace(material.mainTexture), 1, 1, 1));

            retrainButton.animations[0].SetAnim(normal.BuildUVAnim(new Vector2(0f, BUTTON_HEIGHT * 2f).PixelCoordToUVCoord(material.mainTexture), new Vector2(BUTTON_WIDTH, BUTTON_HEIGHT).PixelSpaceToUVSpace(material.mainTexture), 1, 1, 1));
            retrainButton.animations[1].SetAnim(normal.BuildUVAnim(new Vector2(BUTTON_WIDTH, BUTTON_HEIGHT * 2f).PixelCoordToUVCoord(material.mainTexture), new Vector2(BUTTON_WIDTH, BUTTON_HEIGHT).PixelSpaceToUVSpace(material.mainTexture), 1, 1, 1));

            retireButton.animations[0].SetAnim(normal.BuildUVAnim(new Vector2(0f, BUTTON_HEIGHT * 3f).PixelCoordToUVCoord(material.mainTexture), new Vector2(BUTTON_WIDTH, BUTTON_HEIGHT).PixelSpaceToUVSpace(material.mainTexture), 1, 1, 1));
            retireButton.animations[1].SetAnim(normal.BuildUVAnim(new Vector2(BUTTON_WIDTH, BUTTON_HEIGHT * 3f).PixelCoordToUVCoord(material.mainTexture), new Vector2(BUTTON_WIDTH, BUTTON_HEIGHT).PixelSpaceToUVSpace(material.mainTexture), 1, 1, 1));


            RepositionStatSliders();


            var bc = transform.Find("bg").collider as BoxCollider;
            float z = transform.Find("bg/bg_over").position.z;

            var container = GetComponent<UIListItemContainer>();


            // retrain button
            retrainButton.transform.position = new Vector3(transform.position.x + bc.size.x - 3f, transform.position.y - bc.size.y * 0.5f, z);
            container.MakeChild(retrainButton.gameObject);
            retrainButton.SetValueChangedDelegate(RetrainClick);

            // fire button
            fireButton.transform.position = new Vector3(transform.position.x + bc.size.x - 3f, transform.position.y - bc.size.y * 0.5f - BUTTON_HEIGHT, z);
            container.MakeChild(fireButton.gameObject);
            fireButton.SetValueChangedDelegate(FireClick);

            // retire button
            retireButton.transform.position = new Vector3(transform.position.x + bc.size.x - 3f, transform.position.y - bc.size.y * 0.5f + BUTTON_HEIGHT, z);
            container.MakeChild(retireButton.gameObject);
            retireButton.SetValueChangedDelegate(RetireClick);

            //us.gameObject.BroadcastMessage("Update");
            container.GetComponentInParent<UIScrollList>().PositionItems(); // otherwise the buttons may not be clipped properly until
                                                                     // the list is scrolled or another item added
        }



        /// <summary>
        /// Moves the stat sliders to the left to make room for the extra buttons we'll be using
        /// </summary>
        private void RepositionStatSliders()
        {
            var box = transform.Find("bg").collider as BoxCollider;
            
            var sliderStupidity = transform.Find("bg/slider_stupidity");
            var stupidityLabel = transform.Find("bg/label_stupidity");

            var sliderCourage = transform.Find("bg/slider_courage");
            var courageLabel = transform.Find("bg/label_courage");

            // move courage into center
            float originalx = sliderCourage.position.x;
            sliderCourage.position = new Vector3(transform.position.x + box.size.x * SliderAdjustmentMultiplier, sliderCourage.position.y, sliderCourage.position.z);

            courageLabel.Translate(new Vector3(sliderCourage.position.x - originalx, 0f, 0f), Space.Self);

   
            // move stupidity adjacent to courage
            var courageWidth = sliderCourage.GetComponent<UISlider>().width;

            originalx = sliderStupidity.position.x;
            sliderStupidity.position = new Vector3(sliderCourage.position.x + courageWidth + 5f, sliderStupidity.position.y, sliderStupidity.position.z);
            stupidityLabel.Translate(new Vector3(sliderStupidity.position.x - originalx, 0f, 0f), Space.Self);
        }


        private UIButton CreateButton(string name, Material material)
        {
            var uiCamera = Camera.allCameras.ToList().Find(c => c.name == "EZGUI Cam");
            if (uiCamera == null) Log.Error("failed to find EZGUI cam");

            UIButton button = UIButton.Create(name, Vector3.zero);
            button.SetAnchor(SpriteRoot.ANCHOR_METHOD.MIDDLE_RIGHT);
            button.renderCamera = uiCamera;
            button.gameObject.layer = transform.Find("bg").gameObject.layer;
            button.Setup(BUTTON_WIDTH, BUTTON_HEIGHT, material);

            return button;
        }


        private void DoLogic(IDismissImplementor implementor)
        {
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            {
                implementor.Dismiss(GetComponent<CrewItemContainer>().GetCrewRef());
            }
            else implementor.SpawnConfirmation(GetComponent<CrewItemContainer>());
        }

        private void FireClick(IUIObject obj)
        {
            Log.Debug("MultiButton.FireClick");

            DoLogic(fire);
        }

        private void RetrainClick(IUIObject obj)
        {
            Log.Debug("MultiButton.RetrainClick");

            DoLogic(retrain);
        }

        private void RetireClick(IUIObject obj)
        {
            Log.Debug("MultiButton.RetireClick");

            DoLogic(retire);
        }
    }
}
