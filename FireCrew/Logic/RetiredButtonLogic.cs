﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using ReeperCommon;
using UnityEngine;

namespace FireCrew.Logic
{
    /// <summary>
    /// Contains logic which moves retirees back into active service
    /// </summary>
    class RetiredButtonLogic : MonoBehaviour
    {
        void Start()
        {
            Log.Debug("RetiredButtonLogic.Start: changing button for " + GetComponent<CrewItemContainer>().GetCrewRef().name);

            GetComponent<ButtonListener>().Button = ButtonListener.ButtonType.Check;
            GetComponent<ButtonListener>().OnClick += OnRetiredClick;
        }

        void OnRetiredClick(ProtoCrewMember crew)
        {
            if (FireCrew.Retirees.Contains(crew))
            {
                crew.rosterStatus = ProtoCrewMember.RosterStatus.Available;
                crew.type = ProtoCrewMember.KerbalType.Crew;

                FireCrew.Instance.RemoveRetired(crew);
                FireCrew.Retirees.Remove(crew);
                FireCrew.Instance.AddAvailable(PrefabTweaker.CreateItem(crew, "Out of retirement", ButtonListener.ButtonType.X));
            }
            else Log.Warning("RetiredButtonLogic: Tried to retire {0} but not contained in retiree list");
        }
    }
}
