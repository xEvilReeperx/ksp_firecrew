﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using ReeperCommon;
using UnityEngine;

namespace FireCrew.Logic
{
    using UIControls;
    using DismissStyles;

    /// <summary>
    /// This particular button logic makes use of the "X" button on the available list to do one of
    /// three things: fire, retrain or retire a kerbal (defined by config). 
    /// 
    /// Note: A different component is used for the multi option with buttons for each of these things.
    /// </summary>
    [RequireComponent(typeof(ButtonListener))]
    [RequireComponent(typeof(CrewItemContainer))]
    class AvailableButtonLogic : MonoBehaviour
    {
        IDismissImplementor implementor;

        /******************************************************************************
         *                    Implementation Details
         ******************************************************************************/

        private void Start()
        {
            GetComponent<ButtonListener>().OnClick += OnClick;
            GetComponent<ButtonListener>().Button = ButtonListener.ButtonType.X;

            switch (FireCrew.DismissType)
            {
                case FireCrew.DismissTypes.Fire:
                    implementor = new FireKerbal();
                    break;

                case FireCrew.DismissTypes.Retire:
                    implementor = new RetireKerbal();
                    break;

                case FireCrew.DismissTypes.Retrain:
                    implementor = new RetrainKerbal();
                    break;

                default:
                    Log.Error("CRITICAL: Somehow AvailableButtonListener is initializing with an invalid DismissType, going with default fire option");
                    implementor = new FireKerbal();
                    break;
            }
        }



        private void OnClick(ProtoCrewMember crew)
        {
            Log.Debug("AvailableButtonListener.OnClick with " + crew.name);

            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) // if control is held, bypass confirmation
            {
                implementor.Dismiss(crew);
            }
            else
            {
                implementor.SpawnConfirmation(GetComponent<CrewItemContainer>());
            }
        }
    }
}
