﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using UnityEngine;
using ReeperCommon;

namespace FireCrew.Logic
{
    
    /// <summary>
    /// Overwrites the existing container logic and replaces it with a generic event that
    /// other code (that will be 'logic plugins' essentially) can hook into to change the
    /// default behaviour
    /// </summary>
    [RequireComponent(typeof(CrewItemContainer))]
    class ButtonListener : MonoBehaviour
    {
        UIButton close;
        UIButton accept;
        BTButton bg;                    // normal background
        BTButton bg_over;               // highlight background

        public enum ButtonType
        {
            Check,
            X,
            None
        }
        ButtonType button = ButtonType.None;
        


        Rect dlgRect = new Rect(0f, 0f, 260f, 115f);
        bool flag = false;              // flag set when the button is moused over

        public delegate void ButtonClickedDelegate(ProtoCrewMember crew);
        public event ButtonClickedDelegate OnClick = delegate { };



        /******************************************************************************
         *                    Implementation Details
         ******************************************************************************/

        //private System.Collections.IEnumerator Start()
        private void Start()
        {
            CrewItemContainer cic = GetComponent<CrewItemContainer>(); if (cic == null) Log.Error("Failed to find CrewItemContainer");

            close = cic.transform.FindChild("bg/bg_over/button_close").GetComponent<UIButton>();
            accept = cic.transform.Find("bg/bg_over/button_accept").GetComponent<UIButton>();

            bg = cic.transform.FindChild("bg").GetComponent<BTButton>();
            bg_over = cic.transform.FindChild("bg/bg_over").GetComponent<BTButton>();

            // The button delegate needs to be overwritten because it already has
            // logic that will remove the item from the list when clicked. That's bad;
            // we want to give the player a chance to cancel or confirm
            close.SetInputDelegate(ButtonInput);
            accept.SetInputDelegate(ButtonInput);

            bg.SetInputDelegate(BackgroundInput);
            bg_over.SetInputDelegate(BackgroundInput);
            SetButton(Button);
        }



        /// <summary>
        /// Delegate called when the "delete" button is pressed
        /// </summary>
        /// <param name="ptr"></param>
        private void ButtonInput(ref POINTER_INFO ptr)
        {
            switch (ptr.evt)
            {
                case POINTER_INFO.INPUT_EVENT.MOVE_OFF:
                    flag = false;
                    BackgroundInput(ref ptr);

                    break;

                case POINTER_INFO.INPUT_EVENT.MOVE:
                    flag = true;
                    //UIManager.instance.FocusObject = close;
                    BackgroundInput(ref ptr);

                    break;

                case POINTER_INFO.INPUT_EVENT.TAP:
                    Log.Debug("ButtonListener.Click");
                    OnClick(GetComponent<CrewItemContainer>().GetCrewRef());

                    break;

            }
        }



        /// <summary>
        /// The background of the kerbal entry seems to be made up of two items:
        /// a background button and a "bg_over" button that's slightly less wide
        /// to accomodate the right-hand button. Their states seem to be managed
        /// by the input delegate we overwrite in Start, so it falls to us to
        /// manage their states as well
        /// </summary>
        /// <param name="ptr"></param>
        private void BackgroundInput(ref POINTER_INFO ptr)
        {
            switch (ptr.evt)
            {
                case POINTER_INFO.INPUT_EVENT.MOVE:
                    bg_over.gameObject.SetActive(true);
                    bg.renderer.enabled = false;

                    if (Button == ButtonType.X)
                    {
                        close.gameObject.SetActive(true);
                        close.renderer.enabled = true;
                    }
                    else if (Button == ButtonType.Check)
                    {
                        accept.gameObject.SetActive(true);
                        accept.renderer.enabled = true;
                    }
                    break;

                case POINTER_INFO.INPUT_EVENT.MOVE_OFF:
                    if (!flag)
                    {
                        bg_over.gameObject.SetActive(false);
                        bg.renderer.enabled = true;

                        if (Button == ButtonType.X)
                        {
                            close.gameObject.SetActive(false);
                        }
                        else if (Button == ButtonType.Check)
                            accept.gameObject.SetActive(false);

                        flag = false;
                    }

                    break;
            }
        }



        /// <summary>
        /// When the EzGUI is locked by the confirmation dialog, the listener won't reset
        /// the container state like we'd like so we need to provide a way to reset its state
        /// </summary>
        public void Reset()
        {
            POINTER_INFO ptr = new POINTER_INFO();
            ptr.evt = POINTER_INFO.INPUT_EVENT.MOVE_OFF;

            ButtonInput(ref ptr);
            BackgroundInput(ref ptr);
        }



        private void DisableButton()
        {
            Log.Debug("ButtonListener.DisableButton");

            bg_over.gameObject.SetActive(false);
            bg.SetInputDelegate(delegate { });

            accept.gameObject.SetActive(false);
            close.gameObject.SetActive(false);
        }



        private void SetButton(ButtonType b)
        {
            button = b;
            
            switch (button)
            {
                case ButtonType.Check:
                    GetComponent<CrewItemContainer>().SetButton(CrewItemContainer.ButtonTypes.V);
                    break;

                case ButtonType.X:
                    GetComponent<CrewItemContainer>().SetButton(CrewItemContainer.ButtonTypes.X);
                    break;

                case ButtonType.None:
                    DisableButton();
                    // we just disabled this
                    break;
            }

            //Log.Debug("Setting {0} button to {1}", GetComponent<CrewItemContainer>().GetCrewRef().name, b.ToString());
        }



        #region properties


        public ButtonType Button
        {
            get
            {
                return button;
            }
            set
            {
                if (value != button)
                    SetButton(value);
            }
        }

        #endregion
    }
}
