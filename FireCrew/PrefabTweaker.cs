﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using System.Linq;
using ReeperCommon;
using UnityEngine;

namespace FireCrew
{
    /// <summary>
    /// This class locates the EzGUI GameObject container prefabs that are used for the two different
    /// types of list in the astronaut complex (the applicant container is not as wide) and adds a 
    /// component to it that will completely overwrite the logic KSP normally uses there. We could
    /// just overwrite the lists we're interested in but this is cleaner and more consistent.
    /// </summary>
    class PrefabTweaker : MonoBehaviour
    {
        /******************************************************************************
         *                    Implementation Details
         ******************************************************************************/

        void Awake()
        {
            if (Ready) return;

            EnlistedPrefab = Resources.FindObjectsOfTypeAll<GameObject>().Where(go => go.name == "widget_crew_enlisted").SingleOrDefault();
            ApplicantPrefab = Resources.FindObjectsOfTypeAll<GameObject>().Where(go => go.name == "widget_crew_applicants").SingleOrDefault();

            if (EnlistedPrefab == null) Log.Error("PrefabTweaker: Failed to find enlisted item prefab!");
            if (ApplicantPrefab == null) Log.Error("PrefabTweaker: Failed to find applicant item prefab!");

            TweakPrefabs();
        }


        private void TweakPrefabs()
        {
            if (!Ready) { Log.Error("Can't edit prefabs"); return; }

            if (EnlistedPrefab.GetComponent<Logic.ButtonListener>() != null)
            {
                Log.Error("Enlisted already has a ButtonListener!");
            }
            else
            {
                // overwrite the default logic with our own. It's more consistent this way
                EnlistedPrefab.AddComponent<Logic.ButtonListener>();
                ApplicantPrefab.AddComponent<Logic.ButtonListener>();
            }
#if DEBUG
            Log.Debug("EnlistedPrefab:");
            EnlistedPrefab.PrintComponents();
#endif

            Log.Normal("PrefabTweaker ran successfully");
        }




        public static IUIListObject CreateItem(ProtoCrewMember crew, string label, Logic.ButtonListener.ButtonType button)
        {

            GameObject item;

            if (crew.type == ProtoCrewMember.KerbalType.Applicant)
            {
                item = (GameObject)Instantiate(ApplicantPrefab);
                Log.Debug("PrefabTweaker: Creating applicant prefab");
            }
            else
            {
                item = (GameObject)Instantiate(EnlistedPrefab);
                Log.Debug("PrefabTweaker: Creating enlisted prefab");
            }

            var cic = item.GetComponent<CrewItemContainer>();

            cic.Start(); // else an exception is thrown inside StartSliders
            cic.StartSliders();
            item.SetActive(true);


            cic.SetStats(crew.courage, crew.stupidity);
            cic.SetKerbal(crew.type == ProtoCrewMember.KerbalType.Applicant ? CrewItemContainer.KerbalTypes.RECRUIT : CrewItemContainer.KerbalTypes.AVAILABLE);
            if (crew.type != ProtoCrewMember.KerbalType.Applicant)
                cic.SetKerbalAsVeteranIfApplicable(crew);
            cic.SetName(crew.name);
            cic.SetLabel(label);
            cic.SetCrewRef(crew);

            item.GetComponent<Logic.ButtonListener>().Button = button;

            return cic.GetContainer();
        }


        #region properties

        public static GameObject EnlistedPrefab { private set; get; }
        public static GameObject ApplicantPrefab { private set; get; }
        public static bool Ready { get { return EnlistedPrefab != null && ApplicantPrefab != null; } }


        #endregion
    }
}
