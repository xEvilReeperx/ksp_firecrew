﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using ReeperCommon;
using UnityEngine;

namespace FireCrew.DismissStyles
{
    using UIControls;

    class FireKerbal : IDismissImplementor
    {

/******************************************************************************
 *                    Implementation Details
 ******************************************************************************/


        public virtual void Dismiss(ProtoCrewMember crew)
        {
            FireCrew.Instance.RemoveAvailable(crew);

            Log.Normal("Firing {0} permanently.", crew.name);

            crew.SetMaxRespawn();
            FireCrew.Firees.Add(crew);
        }



        public virtual void SpawnConfirmation(CrewItemContainer cic)
        {
            ConfirmationPopup.CreatePopup(cic, DrawPopup);
        }



        protected virtual void DrawPopup()
        {
            GUILayout.TextArea(string.Format("Are you sure you want to fire {0}?", ConfirmationPopup.ActiveInstance.Crew.name), HighLogic.Skin.label);

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Confirm"))
                {
                    Dismiss(ConfirmationPopup.ActiveInstance.Crew);
                    ConfirmationPopup.Dismiss(); 
                }

                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Cancel"))
                {
                    ConfirmationPopup.Dismiss();
                }
            }
            GUILayout.EndHorizontal();
        }
    }
}
