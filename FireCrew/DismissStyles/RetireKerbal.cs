﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using ReeperCommon;
using UnityEngine;

namespace FireCrew.DismissStyles
{
    using UIControls;

    class RetireKerbal : IDismissImplementor
    {
        public void Dismiss(ProtoCrewMember crew)
        {
            Log.Normal("Retiring " + crew.name);

            crew.type = ProtoCrewMember.KerbalType.Crew;
            crew.SetMaxRespawn();

            var item = PrefabTweaker.CreateItem(crew, "Just retired", Logic.ButtonListener.ButtonType.Check);

            FireCrew.Instance.RemoveAvailable(crew);
            FireCrew.Instance.AddRetired(item);
            FireCrew.Retirees.Add(crew);
        }



        public void SpawnConfirmation(CrewItemContainer cic)
        {
            ConfirmationPopup.CreatePopup(cic, DrawPopup);
        }



        /// <summary>
        /// Draws section of the popup relevant for retiring a kerbal
        /// </summary>
        private void DrawPopup()
        {
            GUILayout.TextArea("Are you sure you want to retire this astronaut?", HighLogic.Skin.label);

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Confirm"))
                {
                    Dismiss(ConfirmationPopup.ActiveInstance.Crew);
                    ConfirmationPopup.Dismiss();
                }

                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Cancel"))
                {
                    ConfirmationPopup.Dismiss();
                }
            }
            GUILayout.EndHorizontal();
        }
    }
}
