﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using ReeperCommon;
using UnityEngine;

namespace FireCrew.UIControls
{
    class ListInterface
    {
        public delegate void ListChangedDelegate();

        // events
        public event ListChangedDelegate OnListChanged = delegate { };

        // members
        UIScrollList scroll;
        HashSet<Type> buttonAttachments = new HashSet<Type>();              // list of types to be attached to all ListItemContainers
                                                                            // this is an easy way to let a caller customize the appearance
                                                                            // of each ListItemContainer without knowing any internal details

        /******************************************************************************
         *                    Implementation Details
         ******************************************************************************/

        #region static methods

        /// <summary>
        /// Returns a proxy object that hides the exact ugly details of the implementation from
        /// the caller
        /// </summary>
        /// <param name="scrollName"></param>
        /// <param name="panelName"></param>
        /// <returns></returns>
        public static ListInterface GetList(string panelName)
        {
            Log.Debug("ListInterface.GetList with panelName " + panelName);

            var targetPanel = UIPanelManager.instance.GetPanels().SingleOrDefault(uipb => string.Equals(panelName, uipb.name));
            if (targetPanel == null)
            {
                // treat the name as a full path, if the target panel isn't handled by the UIPanelManager
                var child = UIManager.instance.transform.Find(panelName);

                if (child == null || child.GetComponent<UIScrollList>() == null)
                {
                    Log.Error("ProxyList: failed to find '{0}'", panelName);
                    Log.Normal("Panels:");
                    foreach (var panel in UIPanelManager.instance.GetPanels())
                        Log.Normal("Panel: " + panel.name);

                    return null;
                }
                else return new ListInterface(child.GetComponent<UIScrollList>());
            }


            return new ListInterface(targetPanel);
        }



        public static ListInterface GetList(GameObject panelGo)
        {
            Log.Debug("ListInterface.GetList with GameObject " + panelGo.name);

            //var targetPanel = panelGo.GetComponent<UIPanel>();

            return new ListInterface(panelGo.GetComponent<UIScrollList>() ?? panelGo.transform.GetChild(0).GetComponent<UIScrollList>());
        }

        #endregion



        /// <summary>
        /// It's a constructor. It constructs. 
        /// </summary>
        /// <param name="scrolllist"></param>
        /// <param name="p"></param>
        ListInterface(UIPanelBase p)
        {
            scroll = p.GetComponentInChildren<UIScrollList>();
            
            if (scroll != null)
            {
                scroll.AddValueChangedDelegate(ScrollListValueChanged);
            }
            else Log.Warning("{0} has no scrolllist attached", p.name);

#if DEBUG
            //Log.Write("ProxyList: " + p.name);
            //p.gameObject.PrintComponents();
#endif
        }



        /// <summary>
        /// It constructs. But from a UIScrollList this time, since the applicant list doesn't
        /// have a panel like the other lists do
        /// </summary>
        /// <param name="scrolllist"></param>
        ListInterface(UIScrollList scrolllist)
        {
            scroll = scrolllist;
            scroll.AddValueChangedDelegate(ScrollListValueChanged);
            
#if DEBUG
            scroll.AddDragDropDelegate(delegate(EZDragDropParams param) { Log.Warning("drag drop delegate"); });

            //Log.Write("ProxyList (UIScrollList): " + scroll.name);
            //scroll.gameObject.PrintComponents();
#endif
        }



        /// <summary>
        /// Cleans up after itself
        /// </summary>
        ~ListInterface()
        {
            if (scroll != null) scroll.RemoveValueChangedDelegate(ScrollListValueChanged);
        }



        /// <summary>
        /// Adds a type that will be attached to each ListItemContainer
        /// </summary>
        /// <param name="ty"></param>
        public void AddItemAttachment(Type ty)
        {
            buttonAttachments.Add(ty);
            UpdateAttachments();
        }



        /// <summary>
        /// Makes sure that all item containers in this list contain all specified component
        /// attachments
        /// </summary>
        public void UpdateAttachments()
        {
            if (scroll != null)
            {
                foreach (var cic in scroll.GetComponentsInChildren<CrewItemContainer>(true))
                {
                    // add any extra types to this button
                    foreach (var t in buttonAttachments)
                        if (cic.gameObject.GetComponent(t) == null)
                        {
#if DEBUG
                            Log.Debug("{0} did not have a ButtonListener attached", cic.GetCrewRef().name);
#endif
                            cic.gameObject.AddComponent(t);
                        }
                }

                // KSP seems to run a bit of logic a frame after creating CrewItemContainers, overwriting
                // our changes. We need some way to let ButtonListeners know when the list has changed
                // without them knowing which list they're subscribed to. This primarily fixes
                // a bug where our logic is overwritten right as the gui starts for the first time in
                // a scene
                //foreach (Logic.ButtonListener bl in scroll.GetComponentsInChildren<Logic.ButtonListener>(true))
                //    if (bl.gameObject.activeSelf)
                //        bl.OnEnable();
            }
        }



        /// <summary>
        /// Remove all items from the list (destroys their GameObjects)
        /// </summary>
        public void ClearList()
        {
            if (scroll != null)
            {
                scroll.ClearList(true);
                ResizeScrollbar();
            } else Log.Error("ListInterface.ClearList: scroll is null");
        }



        public void Refresh()
        {
            if (scroll != null)
            {
                UpdateAttachments();
                scroll.PositionItems();
                ResizeScrollbar();
                OnListChanged();
            }
            else Log.Debug("Can't refresh; scroll null");
        }



        /// <summary>
        /// Remove a single item from the list
        /// </summary>
        /// <param name="item"></param>
        /// <param name="destroy"></param>
        /// <param name="easing"></param>
        public void RemoveItem(IUIListObject item, bool destroy = true, bool easing = true)
        {
            var oldPos = scroll.ScrollPosition;
            scroll.RemoveItem(item, destroy, easing);
            ResizeScrollbar();
            scroll.ScrollListTo(oldPos);
        }



        /// <summary>
        /// Add a single item to the list
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(IUIListObject item)
        {
            scroll.AddItem(item);
            UpdateAttachments();
            ResizeScrollbar();
            scroll.RepositionItems();
        }



        public IUIListObject FindItem(ProtoCrewMember crew)
        {
            for (int i = 0; i < scroll.Count; ++i)
                if (scroll.GetItem(i).gameObject.GetComponent<CrewItemContainer>().GetCrewRef() == crew)
                    return scroll.GetItem(i);
            return null;
        }


        #region internal


        /// <summary>
        /// Fixes a bug that would cause the thumb knob on the scrollbar to be the wrong size
        /// whenever items were added or removed and it was needed
        /// </summary>
        public void ResizeScrollbar()
        {
            //var cmsl = scroll.GetComponentInParent<CMScrollList>();
            //if (cmsl != null)
            if (scroll.GetComponent<CMScrollList>() != null)
            {
                //scroll.GetComponentInParent<CMScrollList>().ResizeScrollbar(); // this might occur if the list was just created
                scroll.GetComponent<CMScrollList>().ResizeScrollbar();
            }
#if DEBUG
            else Log.Error("ResizeScrollbar failed on {0}", scroll.name);
#endif                                                                         // and hasn't been Started yet
        }



        /// <summary>
        /// If this ProxyList was assigned a scroll list, this method will be called whenever
        /// its contents is changed (items added, removed) which in turn triggers an event
        /// </summary>
        /// <param name="o"></param>
        private void ScrollListValueChanged(IUIObject o)
        {
            Log.Debug("ProxyList.ScrollListValueChanged " + o.name);

            UpdateAttachments();

            OnListChanged();
        }


        #endregion


        #region properties

        public string Name
        {
            get
            {
                return scroll != null ? scroll.name : "<ListInterface not ready>";
            }
        }



        public int Count
        {
            get
            {
                if (scroll == null) return 0;
                return scroll.Count;
            }
        }



        public IUIListObject this[int idx]
        {
            get
            {
                if (scroll == null) return null;
                return scroll.GetItem(idx);
            }
        }

        public UIPanel Panel
        {
            get
            {
                return scroll.transform.parent.GetComponent<UIPanel>();
            }
        }
        #endregion

    }
}
