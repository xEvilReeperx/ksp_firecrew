﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using UnityEngine;
using ReeperCommon;

namespace FireCrew.UIControls
{
    class ConfirmationPopup
    {
        Rect dlgRect = new Rect(0f, 0f, 260f, 115f);
        Texture2D portrait;
        PopupDialog popup;
        Callback customDraw;

        /******************************************************************************
         *                    Implementation Details
         ******************************************************************************/

        #region static

        public static ConfirmationPopup CreatePopup(CrewItemContainer cic, Callback draw)
        {
            if (ActiveInstance != null)
            {
                Log.Warning("ConfirmationPopup.CreatePopup: A confirmation popup is already open; canceling it");
                Dismiss();
            }

            return new ConfirmationPopup(cic, draw);   
        }

        public static void Dismiss()
        {
            if (ActiveInstance != null)
                ActiveInstance.Close();
        }

        #endregion


        #region members

        private ConfirmationPopup(CrewItemContainer cic, Callback drawMethod)
        {
            // we'll prettify the dialog by showing a picture of the kerbal; we'll
            // need the texture and coordinates to manage that
            ItemContainer = cic;

            var kerbalPic = cic.gameObject.transform.FindChild("bg/kerbal").gameObject.GetComponent<UIStateToggleBtn>();
            var sourceTexture = ((Texture2D)kerbalPic.renderer.sharedMaterial.mainTexture).CreateReadable();
            Rect uv = kerbalPic.GetUVs();

            // uvs are normalized; unity expects screen space
            uv.x *= sourceTexture.width;
            uv.y *= sourceTexture.height;
            uv.width *= sourceTexture.width;
            uv.height *= sourceTexture.height;

            portrait = new Texture2D((int)uv.width, (int)uv.height, TextureFormat.ARGB32, false);
            portrait.filterMode = FilterMode.Trilinear;

            var pixels = sourceTexture.GetPixels((int)uv.x, (int)uv.y, (int)uv.width, (int)uv.height);
            portrait.SetPixels(pixels);
            portrait.Apply();
            Texture2D.DestroyImmediate(sourceTexture);

            var mod = new MultiOptionDialog(null, new Callback(PartialDraw), "Confirmation Needed", FireCrew.Skin);
            mod.dialogRect = dlgRect;
            mod.dialogRect.x = Screen.width / 2 - mod.dialogRect.width / 2;
            mod.dialogRect.y = Screen.height / 2 - mod.dialogRect.height / 2;

            customDraw = drawMethod;
            Crew = cic.GetCrewRef();

            ActiveInstance = this;
            InputLockManager.SetControlLock(ControlTypes.ACTIONS_ALL, "FireKerbal");
            UIManager.instance.LockInput();

            popup = PopupDialog.SpawnPopupDialog(mod, false, FireCrew.Skin);
            popup.skin = FireCrew.Skin;

            popup.gameObject.AddComponent<HighlightCrew>().Setup(cic);
        }


        public void Close()
        {
            popup.Dismiss();
            ActiveInstance = null;
            popup = null;

            InputLockManager.RemoveControlLock("FireKerbal");
            UIManager.instance.UnlockInput();

            if (ItemContainer != null)
                if (ItemContainer.GetComponent<Logic.ButtonListener>() != null)
                    ItemContainer.GetComponent<Logic.ButtonListener>().Reset();
        }



        private void PartialDraw()
        {
            GUILayout.BeginVertical();
            {
                GUILayout.BeginHorizontal();
                {
                    GUILayout.Label(string.Empty, HighLogic.Skin.label, GUILayout.Width(portrait.width * 1.3f), GUILayout.Height(portrait.height * 1.3f));
                    GUI.DrawTexture(GUILayoutUtility.GetLastRect(), portrait, ScaleMode.StretchToFill);

                    // center kerbal name
                    GUILayout.BeginVertical();
                    {
                        GUI.contentColor = Color.white;
                        GUILayout.FlexibleSpace();
                        GUILayout.Label(Crew.name);
                        GUILayout.FlexibleSpace();
                    }
                    GUILayout.EndVertical();
                }
                GUILayout.EndHorizontal();

                customDraw();

            }
            GUILayout.EndVertical();
        }

        #endregion



        #region properties

        public static ConfirmationPopup ActiveInstance { private set; get; }
        public ProtoCrewMember Crew { private set; get; }
        public CrewItemContainer ItemContainer { private set; get; }

        #endregion
    }



    /// <summary>
    /// A little GUI sugar
    /// </summary>
    class HighlightCrew : MonoBehaviour
    {
        BTButton bg;                 
        BTButton bg_over;

        Material originalMat;
        Material mat;

        Renderer target;

        public void Setup(CrewItemContainer cic)
        {
            bg = cic.transform.FindChild("bg").GetComponent<BTButton>();
            bg_over = cic.transform.FindChild("bg/bg_over").GetComponent<BTButton>();

            if (bg == null || bg_over == null)
            {
                Component.Destroy(this);
            }
            else
            {
                Log.Debug("Running highlight on {0} container", cic.GetCrewRef().name);
                StartCoroutine(RunHighlight());
            }
        }



        /// <summary>
        /// A little routine to make it clear which kerbal is about to be dismissed
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator RunHighlight()
        {
            target = bg_over.gameObject.activeInHierarchy ? bg_over.renderer : bg.renderer;

            originalMat = target.sharedMaterial;
            mat = Material.Instantiate(originalMat) as Material;

            Color dark = new Color(0.5f, 0.5f, 0.5f);
            Color light = new Color(0.7f, 0.7f, 0.7f);

            float time = 0f;

            target.material = mat;

            while (true)
            {
                if (time < 1f)
                {
                    mat.SetColor("_Color", Color.Lerp(dark, light, time));
                }
                else mat.SetColor("_Color", Color.Lerp(light, dark, time - 1f));

                while (time > 2f) time -= 2f;

                yield return 0;
                time += Time.deltaTime;
            }
        }

        void OnDestroy()
        {
            if (bg_over != null && bg_over.renderer != null && target != null) // since it's possible the item was removed from the list before popup was dismissed
                target.sharedMaterial = originalMat;

            Material.DestroyImmediate(mat);
        }
    }
}
