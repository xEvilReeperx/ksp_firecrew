﻿/******************************************************************************
 *             FireCrew for Kerbal Space Program                 *
 *                                                                            *
 * Version 1.1                                                                *
 * Created: 8/27/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/
using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using ReeperCommon;
using UnityEngine;

namespace FireCrew
{
    using UIControls;

    [KSPScenario(ScenarioCreationOptions.AddToAllGames, GameScenes.EDITOR, GameScenes.SPACECENTER, GameScenes.SPH)]
    class FireCrew : ScenarioModule
    {
        public static readonly string ConfigPath = ConfigUtil.GetDllDirectoryPath() + "/config.cfg";
        private const float NewWidthRatio = .85f;

        /// <summary>
        /// There are two basic ways the addon can handle UI: either the single "delete" style button
        /// which does whatever single option is defined in the config, or by adding a button for each
        /// dismissal type directly into the list item itself
        /// </summary>
        public enum UIStyle
        {
            SingleOption,                   // use the red x button and do only one thing
            MultiOption                     // add buttons to fire, retire and retrain
        }

        /// <summary>
        /// Possible dismissal styles if SingleOption is selected; otherwise it's unused
        /// </summary>
        public enum DismissTypes
        {
            Fire,
            Retrain,
            Retire,
            Unused
        }

        ListInterface applicantList;
        ListInterface availableList;
        ListInterface assignedList;                     // only needed so we can disable buttons
        ListInterface kiaList;
        ListInterface retiredList;                      // only needed if there are any retired kerbals


        // we'll need these to handle spawning astronaut GUI (since we're going to kill the original
        // object that controls it)
        UIPanel topBar;
        UIPanel crewPanels;

        // should be self-evident
        UIPanel retiredPanel;

        GameObject astronautComplex;

        // keep track of "retired" crew; these are actually going to be "dead" kerbals but
        // we'll prevent the player from ever knowing
        List<ProtoCrewMember> retirees = new List<ProtoCrewMember>();

        // similar situation as retirees
        List<ProtoCrewMember> firees = new List<ProtoCrewMember>();


        /******************************************************************************
         *                    Implementation Details
         ******************************************************************************/

        #region initialization/deinit


        public override void OnAwake()
        {
            LoadConfig();
            gameObject.AddComponent<PrefabTweaker>(); // RequireComponent apparently doesn't work on ScenarioModules (?)

            ReplaceStockLogic();
            SetupLists();
            SetupSkin();

            if (PrefabTweaker.Ready)
            {
                GameEvents.onGUIAstronautComplexSpawn.Add(OnEnterGUI);
                Instance = this;
            }
            else
            {// else something went wrong; we don't destroy ourself because we want to preserve any stored persistent data
                Log.Error("FireCrew ScenarioModule: something with wrong with the prefabs");
            }
        }



        /// <summary>
        /// Load user options from config
        /// </summary>
        private void LoadConfig()
        {
            if (System.IO.File.Exists(FireCrew.ConfigPath))
            {
                ConfigNode node = ConfigNode.Load(FireCrew.ConfigPath);

                if (node != null)
                {
                    InterfaceStyle = node.ParseEnum<UIStyle>("UIStyle", UIStyle.MultiOption);
                    Log.Normal("FireCrew interface style set to " + InterfaceStyle.ToString());

                    if (InterfaceStyle == UIStyle.SingleOption)
                    {
                        DismissType = node.ParseEnum<DismissTypes>("DismissType", DismissTypes.Fire);
                        Log.Normal("Single option set to " + DismissType.ToString());
                    }
                    else DismissType = DismissTypes.Unused;

                    Log.LoadFrom(node);
                }
            }
            else
            {
                Log.Warning("Did not find config file. Using defaults");

                InterfaceStyle = UIStyle.MultiOption;
                DismissType = DismissTypes.Unused;
            }
        }



        /// <summary>
        /// The stock logic that controls the lists turned out to be very annoying to work
        /// around so it'll be much easier just to rip it out and use our own instead
        /// </summary>
        private void ReplaceStockLogic()
        {
            if (UIManager.instance == null)
                Log.Error("ReplaceStockLogic: UIManager.instance is null");

            //var complex = UIManager.instance.transform.Find("panel_AstronautComplex");

            // this could be in panel_AstronautComplex or in a child object in the case of the editor scene
            astronautComplex = UIManager.instance.transform.Find("panel_AstronautComplex").GetComponentInChildren<CMAstronautComplex>().gameObject;

            if (astronautComplex == null)
            {
                Log.Error("Failed to find CMAstronautComplex component!");
            }
            else
            {
                // we'll be needing to handle the panels ourselves
                // TopBar
                // CrewPanels
                Log.Normal("FireCrew.OnAwake: Finding UIPanels");

#if DEBUG
                astronautComplex.gameObject.PrintComponents();
#endif

               // topBar = complex.transform.Find("TopBar").GetComponent<UIPanel>();

                // looks like the breaking change for 0.25 is that one item was renamed and
                // the astronaut complex hierarchy is slightly different

                topBar = astronautComplex.transform.Find("GameObject").GetComponent<UIPanel>();
                crewPanels = astronautComplex.transform.Find("CrewPanels").GetComponent<UIPanel>();


                Log.Normal("FireCrew.OnAwake: Destroying CMAstronautComplex");
                Component.Destroy(astronautComplex.GetComponentInChildren<CMAstronautComplex>());
            }
        }



        /// <summary>
        /// The stock HighLogic.Skin has a transparent window background which
        /// makes the popup slightly harder to read over the background, so
        /// create a new style with an opaque background
        /// </summary>
        private void SetupSkin()
        {
            // modify skin a little to make confirmation windows more visible
            // (the default one has some transparency in it)
            if (Skin == null)
            {
                Skin = (GUISkin)Instantiate(HighLogic.Skin);

                Texture2D tex = Skin.window.normal.background.CreateReadable();

                var pixels = tex.GetPixels32();

                for (int i = 0; i < pixels.Length; ++i)
                    pixels[i].a = 255;

                tex.SetPixels32(pixels); tex.Apply();


                Skin.window.onActive.background = tex;
                Skin.window.onFocused.background = tex;
                Skin.window.onNormal.background = tex;
            }
        }



        /// <summary>
        /// Sets up interfaces through which other objects will interact with the
        /// EzGUI lists on the astronaut complex page. It simplifies things slightly
        /// </summary>
        private void SetupLists()
        {
            //////////////////////////////////////////////////
            // Set up applicant list
            //////////////////////////////////////////////////
            //applicantList = ListInterface.GetList("panel_AstronautComplex/CrewPanels/panel_applicants/scrolllist_applicants");

            Log.Normal("Setting up applicant list");
            applicantList = ListInterface.GetList(astronautComplex.GetComponentsInChildren<UIScrollList>(true).Where(uisl => uisl.name == "scrolllist_applicants").Single().gameObject);
            applicantList.ClearList();
            applicantList.AddItemAttachment(typeof(Logic.ApplicantButtonLogic));



            //////////////////////////////////////////////////
            // Set up available list
            //////////////////////////////////////////////////
            Log.Normal("Setting up available list");
            //availableList = ListInterface.GetList("panel_AstronautComplex/CrewPanels/panel_enlisted/panelManager/panel_available/scrolllist_available");
            availableList = ListInterface.GetList(astronautComplex.GetComponentsInChildren<UIScrollList>(true).Where(uisl => uisl.name == "scrolllist_available").Single().gameObject);
            availableList.ClearList();

            if (InterfaceStyle == UIStyle.SingleOption)
            {
                availableList.AddItemAttachment(typeof(Logic.AvailableButtonLogic));
            }
            else
            {
                availableList.AddItemAttachment(typeof(Logic.MultiButton));
                availableList.AddItemAttachment(typeof(Logic.HighlightOnMouseover));
            }




            //////////////////////////////////////////////////
            // Set up assigned list
            //////////////////////////////////////////////////
            Log.Normal("Setting up assigned list");
            //assignedList = ListInterface.GetList("panel_AstronautComplex/CrewPanels/panel_enlisted/panelManager/panel_assigned/scrolllist_assigned");
            assignedList = ListInterface.GetList(astronautComplex.GetComponentsInChildren<UIScrollList>(true).Where(uisl => uisl.name == "scrolllist_assigned").Single().gameObject);
            assignedList.ClearList();



            //////////////////////////////////////////////////
            // Set up mia list
            //////////////////////////////////////////////////
            Log.Normal("Setting up mia list");
            //kiaList = ListInterface.GetList("panel_AstronautComplex/CrewPanels/panel_enlisted/panelManager/panel_kia/scrolllist_kia");
            kiaList = ListInterface.GetList(astronautComplex.GetComponentsInChildren<UIScrollList>(true).Where(uisl => uisl.name == "scrolllist_kia").Single().gameObject);
            kiaList.ClearList();



            //////////////////////////////////////////////////
            // Set up retired list (if applicable)
            //////////////////////////////////////////////////

            if (DismissType == DismissTypes.Retire || InterfaceStyle == UIStyle.MultiOption)
            {
                retiredList = CreateRetiredPanel();
                if (retiredList == null) Log.Error("RetiredList is null");
                retiredList.ClearList();
                retiredList.AddItemAttachment(typeof(Logic.RetiredButtonLogic));
            }
        }



        /// <summary>
        /// Standard destructor
        /// </summary>
        private void OnDestroy()
        {
            if (PrefabTweaker.Ready)
                GameEvents.onGUIAstronautComplexSpawn.Remove(OnEnterGUI);

            Instance = null;
        }



        /// <summary>
        /// We keep retirees and firees as "dead" kerbals instead of creating them ourselves for two
        /// reasons. The first is that the game's ProgressTracker seems to keep a reference
        /// to the ProtoCrewMember in question and destroying any that have a ProgressTracker tree
        /// entry will break things. The second is that other mods may depend on them to keep track
        /// of things (ribbons in particular). Since we have full control of what gets displayed
        /// in the astronaut complex anyway, the user will never know what's really going on
        /// </summary>
        /// <param name="node"></param>
        public override void OnLoad(ConfigNode node)
        {
            base.OnLoad(node);

            retirees.Clear();
            firees.Clear();


            if (node.HasNode("RETIREES"))
            {
                ConfigNode rn = node.GetNode("RETIREES");

                foreach (var name in rn.GetValues("retiree"))
                {
                    // find relevant protocrewmember
                    ProtoCrewMember retiredCrew = HighLogic.CurrentGame.CrewRoster[name];

                    if (retiredCrew != null)
                    {
                        retiredCrew.SetMaxRespawn();
                        retirees.Add(retiredCrew);
                        Log.Verbose("Loaded retiree: {0}", retiredCrew.name);
                    } else Log.Error("OnLoad: Retiree named '{0}' no longer exists", name); // they're gone forever, can't do anything about that
                }
            }

            if (node.HasNode("FIREES"))
            {
                ConfigNode fn = node.GetNode("FIREES");

                foreach (var name in fn.GetValues("firee"))
                {
                    ProtoCrewMember firedCrew = HighLogic.CurrentGame.CrewRoster[name];

                    if (firedCrew != null)
                    {
                        firedCrew.SetMaxRespawn();
                        firees.Add(firedCrew);
                        Log.Verbose("Loaded firee: {0}", firedCrew.name);
                    }
                    else Log.Error("OnLoad: Firee named '{0}' no longer exists", name);
                }
            }
            Log.Normal("Loaded {0} retirees", retirees.Count);
            Log.Normal("Loaded {0} firees", firees.Count);
        }



        /// <summary>
        /// See OnLoad for an explanation of why we're tracking retirees and fired kerbals
        /// </summary>
        /// <param name="node"></param>
        public override void OnSave(ConfigNode node)
        {
            base.OnSave(node);

            var rNode = node.AddNode("RETIREES");
            retirees.ForEach(r => rNode.AddValue("retiree", r.name));

            var fNode = node.AddNode("FIREES");
            firees.ForEach(f => fNode.AddValue("firee", f.name));


            Log.Normal("Saved {0} retirees", retirees.Count);
            Log.Normal("Saved {0} firees", firees.Count);
        }


        #endregion

        #region retired panel


        /// <summary>
        /// It pretty much does what you'd think. Creates a retired panel, a tab for
        /// that panel and repositions the existing tabs to make room for the new one
        /// </summary>
        /// <returns></returns>
        private ListInterface CreateRetiredPanel()
        {
            Log.Debug("CreateRetiredPanel");

            // let's clone the mia panel
            // 
            // there are two separate things we need to deal with
            // * the first is the actual panel itself
            // * the second is the tab that the player clicks on to 
            //   access the panel
            var original = kiaList.Panel.gameObject;

            Log.Debug("Cloning kia panel...");
            var retgo = (GameObject)Instantiate(original, original.transform.position, original.transform.rotation);

            retiredPanel = retgo.GetComponent<UIPanel>();

            retiredPanel.name = "Retired";
            retiredPanel.index = 3;
            retiredPanel.transform.parent = original.transform.parent;

            UIPanelManager.instance.AddChild(retgo);
            retgo.SetActive(false);

            // now deal with the tabs. First, we need to find one to clone
            Log.Debug("Finding tabs");

            // note to self: accessing the complex from the spacecenter results in tabs being at
            //    
            // otherwise, a new GO under panel_AstronautComplex called astronautComplex is created
            //    panel_AstronautComplex/astronautComplex/CrewPanels/panel_enlisted/tabs
            var tabsgo = UIManager.instance.transform.Find("panel_AstronautComplex/CrewPanels/panel_enlisted/tabs") ?? UIManager.instance.transform.Find("panel_AstronautComplex/astronautComplex/CrewPanels/panel_enlisted/tabs");
            if (tabsgo == null)
            {
                Log.Error("failed to find tabs");

                if (UIManager.instance.transform.Find("panel_AstronautComplex") != null)
                {
                    if (UIManager.instance.transform.Find("panel_AstronautComplex/CrewPanels") != null)
                    {
                        Log.Error("  detail: couldn't find either \"panel_enlisted/tabs\" in:");
                        UIManager.instance.transform.Find("panel_AstronautComplex/CrewPanels").PrintHierarchy();
                    }
                    else
                    {
                        Log.Error("  detail: couldn't find CrewPanels GO");
                        UIManager.instance.transform.Find("panel_AstronautComplex").PrintHierarchy();
                    }
                }
                else
                {
                    Log.Error("  detail: couldn't find astronaut complex panel");
                    // don't print hierarchy since it would spam log and wouldn't be particularly helpful;
                    // most likely panel_AstronautComplex has been renamed or moved somewhere
                }
            }

            var tabs = tabsgo.GetComponent<CMPanelTabGroup>().transform;

            var tabAvailable = tabs.Find("tab_available");
            var tabAssigned = tabs.Find("tab_assigned");
            var tabLost = tabs.Find("tab_kia");

            // clone a tab...
            Log.Debug("Cloning tab");
            var rettab = (GameObject)Instantiate(tabLost.gameObject, tabLost.transform.position, tabLost.transform.rotation);
            rettab.transform.Translate(new Vector3(tabLost.transform.position.x - tabAssigned.transform.position.x, 0f, 0f), Space.Self);
            rettab.transform.parent = tabLost.transform.parent;

            // have it open our own tab
            Log.Debug("Setting delegates");

            var rettabButton = rettab.GetComponent<BTPanelTab>();

            rettabButton.panel = retiredPanel;
            rettabButton.panelManager = UIPanelManager.instance;

            rettabButton.SetValueChangedDelegate(delegate { retiredPanel.BringIn(); retiredList.ResizeScrollbar();  });
            rettab.GetComponent<BTPanelTab>().SetInputDelegate(delegate {  });
            rettab.transform.GetChild(1).GetComponent<SpriteText>().Text = "Retired";

            // bugfix: cloning the original tab causes it to mysteriously lose its text
            Log.Debug("Fixing SpriteText problem");
            tabLost.gameObject.AddComponent<FixText>().charSize = tabLost.GetChild(1).GetComponent<SpriteText>().characterSize;
            

            // four tabs don't quite fit so scale them down and shift them over a little
            Transform[] tabList = { tabAvailable, tabAssigned, tabLost, rettab.transform };

            Action<Transform, int> AdjustPosition = delegate(Transform t, int idx)
            {
                // adjust width
                t.localScale = new Vector3(NewWidthRatio, 1f, 1f);
                t.Find("text").localScale = new Vector3(1f / NewWidthRatio, 1f, 1f);

                // adjust position
                Log.Debug("{0} width: {1}", t.name, t.GetComponent<BTPanelTab>().width);
                t.Translate(new Vector3(t.GetComponent<BTPanelTab>().width * (1f - t.localScale.x) * -idx, 0f, -idx), Space.Self);
            };

            Log.Debug("Adjusting tab positions");
            for (int i = 0; i < tabList.Length; ++i) AdjustPosition(tabList[i], i);
            

            Log.Debug("finished");
            retgo.PrintComponents();

            return ListInterface.GetList(retgo);
        }

        #endregion

        #region GameEvents

        /// <summary>
        /// Called when the astronaut complex GUI should open
        /// </summary>
        private void OnEnterGUI()
        {
            Log.Debug("************** ON ENTER GUI ************");

            // I don't know if this is intended, but you can apparently accelerate time and then
            // enter the complex. That sounds like a bad thing to me...
            TimeWarp.SetRate(0, false);

            // in case kerbals respawn
            retirees.ForEach(r => r.SetMaxRespawn());
            firees.ForEach(f => f.SetMaxRespawn());

            HighLogic.CurrentGame.CrewRoster.Update(Planetarium.GetUniversalTime());

            // clear old entries
            applicantList.ClearList();
            availableList.ClearList();
            assignedList.ClearList();
            kiaList.ClearList();
            if (retiredList != null) retiredList.ClearList();

            // add list entries
            // important note to self: apparently the various properties filter out kerbal types
            // that don't fit in; "Crew" does not contain a list of all kerbals for example, but
            // only non-applicants, non-tourist, and non-unowned
            Log.Debug("unowned count: " + HighLogic.CurrentGame.CrewRoster.Unowned.Count());
            Log.Debug("tourist count: " + HighLogic.CurrentGame.CrewRoster.Tourist.Count());
            Log.Debug("crew count: " + HighLogic.CurrentGame.CrewRoster.Crew.Count());
            Log.Debug("applicant count: " + HighLogic.CurrentGame.CrewRoster.Applicants.Count());

            List<ProtoCrewMember> crewList = new List<ProtoCrewMember>();

            crewList.AddRange(HighLogic.CurrentGame.CrewRoster.Crew); // available, assigned, and kia/missing kerbals
            crewList.AddRange(HighLogic.CurrentGame.CrewRoster.Applicants); 
            crewList.AddRange(HighLogic.CurrentGame.CrewRoster.Tourist); // not used yet as far as I know
            crewList.AddRange(HighLogic.CurrentGame.CrewRoster.Unowned); // these are from rescue missions

            retirees.ForEach(r => crewList.Remove(r));
            firees.ForEach(f => crewList.Remove(f));

            // regular, non-retired crew
            foreach (var crew in crewList)
            {
                // bugfix: otherwise kerbals never respawn (apparently HighLogic.CurrentGame.CrewRoster.Update doesn't
                // do this like I thought)
                crew.CheckRespawnTimer(Planetarium.GetUniversalTime(), HighLogic.CurrentGame.Parameters);

                switch (crew.type)
                {
                    case ProtoCrewMember.KerbalType.Applicant:
                        AddApplicant(PrefabTweaker.CreateItem(crew, "For hire", Logic.ButtonListener.ButtonType.Check));
                        break;

                    case ProtoCrewMember.KerbalType.Crew:
                        
                        switch (crew.rosterStatus)
                        {
                            case ProtoCrewMember.RosterStatus.Available:
                                AddAvailable(PrefabTweaker.CreateItem(crew, "Available for next mission", Logic.ButtonListener.ButtonType.X));
                                break;

                            case ProtoCrewMember.RosterStatus.Assigned:
                                string text = "On a mission";

                                //Log.Write("Crew.Assigned: {0} - {1}", crew.name, crew.rosterStatus.ToString());

                                // note: KerbalRef seems to always be null, we'll have to look at the protovessels themselves
                                //if (crew.KerbalRef != null)
                                //{
                                //    if (crew.KerbalRef.InVessel == null)
                                //    {
                                //        text = "Not in a vessel";
                                //    }
                                //    else if (crew.KerbalRef.InVessel.isEVA)
                                //    {
                                //        text = "On EVA";
                                //    }
                                //    else if (!crew.KerbalRef.InVessel.isEVA)
                                //    {
                                //        text = string.Format("Aboard {0}\nSeat: {1}, Part: {2}", crew.KerbalRef.InVessel.vesselName, crew.seat.name, crew.KerbalRef.InPart.partName);
                                //    }
                                //}
                                //else Log.Error("KerbalRef is null!");

                                foreach (var pv in HighLogic.CurrentGame.flightState.protoVessels)
                                {
                                    if (pv.GetVesselCrew().Contains(crew))
                                    {
                                        if (pv.vesselType == VesselType.EVA)
                                        {
                                            text = "On EVA";
                                        } else text = string.Format("Vessel: {0}", pv.vesselName);
                                        break;
                                    }
                                }

                                assignedList.AddItem(PrefabTweaker.CreateItem(crew, text, Logic.ButtonListener.ButtonType.None));
                                break;

                            case ProtoCrewMember.RosterStatus.Dead:
                                kiaList.AddItem(PrefabTweaker.CreateItem(crew, "Deceased", Logic.ButtonListener.ButtonType.None));
                                break;

                            case ProtoCrewMember.RosterStatus.Missing:
                                kiaList.AddItem(PrefabTweaker.CreateItem(crew, "Missing in action", Logic.ButtonListener.ButtonType.None));
                                break;
                        }
                        break;

                    case ProtoCrewMember.KerbalType.Tourist:
                        // let's treat these as assigned for now since I'm not sure you should be able to fire/retire etc them yet
                        assignedList.AddItem(PrefabTweaker.CreateItem(crew, "Tourist", Logic.ButtonListener.ButtonType.None));
                        break;

                    case ProtoCrewMember.KerbalType.Unowned:
                        // could be one of two things: either a kerbal from an offered rescue contract in which case
                        // we show nothing, or it could be a kerbal awaiting rescue which the game displays as "On EVA" so
                        // we'll match that
                        if (HighLogic.CurrentGame.flightState.protoVessels.Any(pv => pv.GetVesselCrew().Contains(crew)))
                        {
                            assignedList.AddItem(PrefabTweaker.CreateItem(crew, "On EVA" /* this is how stock does it */, Logic.ButtonListener.ButtonType.None));
                        }
                        else
                        {
                            Log.Debug("Note: Not displaying unowned crew '{0}' because it is the result of an offered rescue contract", crew.name);
                        }

                        break;
                }
            }

            // retirees
            if (retiredList != null)
            {
                retirees.ForEach(r =>
                    {
                        retiredList.AddItem(PrefabTweaker.CreateItem(r, "Retired", Logic.ButtonListener.ButtonType.Check));
                    });
            }
            else
            {
                if (retirees.Count > 0)
                {
                    // The user may have chosen the multi or retire dismissal methods in the past but has changed
                    // their settings. This shouldn't happen often, but if it does it's safest to let them decide what
                    // to do with the formerly retired crew instead of just flat-out deleting them
                    Log.Warning("FireCrew has retired crew entries but no way to display them. They will be placed into the applicant pool.");

                    retirees.ForEach(r => { r.type = ProtoCrewMember.KerbalType.Applicant; r.rosterStatus = ProtoCrewMember.RosterStatus.Available; });
                    retirees.Clear();

                    OnEnterGUI();
                    return;
                }
            }

            // otherwise no music plays
            MusicLogic.fetch.PauseWithCrossfade(MusicLogic.AdditionalThemes.AstronautComplex);

            // need a control lock here, Alt+F12 says stock uses "astronautComplexFacility"
            // note: apparently this is only for the complex when opened from KSC; if opened
            // within editor, setting control locks will freeze all controls
            if (!HighLogic.LoadedSceneIsEditor)
                InputLockManager.SetControlLock("astronautComplexFacility");

            // open panels (this is actually opening the astronaut interface)
            topBar.BringIn();
            crewPanels.BringIn();

            RenderingManager.ShowUI(false); // else the bottom bar from spacecenter remains visible

            applicantList.ResizeScrollbar();
            availableList.ResizeScrollbar();
            assignedList.ResizeScrollbar();
            kiaList.ResizeScrollbar();
            
            if (retiredList != null) retiredList.ResizeScrollbar();

            GameEvents.onGUIAstronautComplexDespawn.Add(OnExitGUI);
        }


        private void OnExitGUI()
        {
            GameEvents.onGUIAstronautComplexDespawn.Remove(OnExitGUI);
            
            RenderingManager.ShowUI(true);
            if (!HighLogic.LoadedSceneIsEditor)
                InputLockManager.RemoveControlLock("astronautComplexFacility");
        }
        

        #endregion


        #region properties

        public static FireCrew Instance { get; private set; }
        public static UIStyle InterfaceStyle { private set; get; }
        public static DismissTypes DismissType { private set; get; }
        public static GUISkin Skin { get; private set; }
        public static List<ProtoCrewMember> Retirees
        {
            get
            {
                return Instance.retirees;
            }
        }

        public static List<ProtoCrewMember> Firees
        {
            get
            {
                return Instance.firees;
            }
        }

        #endregion



        #region list methods



        public void AddApplicant(IUIListObject item)
        {
            applicantList.AddItem(item);
        }

        
        public void RemoveApplicant(ProtoCrewMember crew)
        {
            Log.Debug("RemoveApplicant: " + crew.name);
            Remove(applicantList, crew);
        }

        public void AddAvailable(IUIListObject item)
        {
            availableList.AddItem(item);
        }




        public void RemoveAvailable(ProtoCrewMember crew)
        {
            Log.Debug("RemoveAvailable: " + crew.name);
            Remove(availableList, crew);
        }



        public void AddRetired(IUIListObject item)
        {
            retiredList.AddItem(item);
        }



        public void RemoveRetired(ProtoCrewMember crew)
        {
            Log.Debug("RemoveRetired: " + crew.name);
            Remove(retiredList, crew);
        }



        private void Remove(ListInterface li, ProtoCrewMember crew)
        {
            var found = li.FindItem(crew);

            if (found == null)
            {
                Log.Error("Cannot remove {0} from {1}: item not found", crew.name, li.Name);
            }
            else
            {
                li.RemoveItem(found);
            }
            li.ResizeScrollbar();
        }
        #endregion
    }



    class FixText : MonoBehaviour
    {
        public float charSize = 12f;

        private System.Collections.IEnumerator Start()
        {
            yield return 0;

            var st = transform.GetChild(1).GetComponent<SpriteText>();

            st.Text = "Lost";
            st.UpdateMesh();
            st.characterSize = charSize;

            Component.Destroy(this);
        }
    }



    public static class ProtoCrewMemberExtensions
    {
        public static void SetMaxRespawn(this ProtoCrewMember crew)
        {
            // note: trying to parse double.MaxValue will throw an exception
            // (which cascade-fails and breaks KSP) so we'll use this workaround
            crew.SetTimeForRespawn(1.7976931348623157E+300);
            crew.rosterStatus = ProtoCrewMember.RosterStatus.Dead;
        }
    }
}

/*
[LOG 02:04:55.589] FireCrew, LISTCONTAINER========================
[LOG 02:04:55.590] FireCrew, UIListItemContainer: widget_crew_applicants(Clone)
[LOG 02:04:55.590] FireCrew, widget_crew_applicants(Clone) has components:
[LOG 02:04:55.591] FireCrew, ...c: UnityEngine.Transform
[LOG 02:04:55.592] FireCrew, ...c: CrewItemContainer
[LOG 02:04:55.592] FireCrew, ...c: UIListItemContainer
[LOG 02:04:55.593] FireCrew, --->bg has components:
[LOG 02:04:55.593] FireCrew, ......c: UnityEngine.Transform
[LOG 02:04:55.594] FireCrew, ......c: BTButton
[LOG 02:04:55.594] FireCrew, ......c: UnityEngine.MeshFilter
[LOG 02:04:55.595] FireCrew, ......c: UnityEngine.MeshRenderer
[LOG 02:04:55.596] FireCrew, ......c: UnityEngine.BoxCollider
[LOG 02:04:55.596] FireCrew, ------>bg_over has components:
[LOG 02:04:55.597] FireCrew, .........c: UnityEngine.Transform
[LOG 02:04:55.598] FireCrew, .........c: BTButton
[LOG 02:04:55.598] FireCrew, .........c: UnityEngine.MeshFilter
[LOG 02:04:55.599] FireCrew, .........c: UnityEngine.MeshRenderer
[LOG 02:04:55.599] FireCrew, .........c: UnityEngine.BoxCollider
[LOG 02:04:55.600] FireCrew, --------->button_close has components:
[LOG 02:04:55.601] FireCrew, ............c: UnityEngine.Transform
[LOG 02:04:55.601] FireCrew, ............c: UIButton
[LOG 02:04:55.602] FireCrew, ............c: UnityEngine.MeshFilter
[LOG 02:04:55.603] FireCrew, ............c: UnityEngine.MeshRenderer
[LOG 02:04:55.603] FireCrew, ............c: UnityEngine.BoxCollider
[LOG 02:04:55.604] FireCrew, --------->button_accept has components:
[LOG 02:04:55.604] FireCrew, ............c: UnityEngine.Transform
[LOG 02:04:55.605] FireCrew, ............c: UIButton
[LOG 02:04:55.606] FireCrew, ............c: UnityEngine.MeshFilter
[LOG 02:04:55.606] FireCrew, ............c: UnityEngine.MeshRenderer
[LOG 02:04:55.607] FireCrew, ............c: UnityEngine.BoxCollider
[LOG 02:04:55.608] FireCrew, ------>kerbal has components:
[LOG 02:04:55.608] FireCrew, .........c: UnityEngine.Transform
[LOG 02:04:55.609] FireCrew, .........c: UIStateToggleBtn
[LOG 02:04:55.609] FireCrew, .........c: UnityEngine.MeshFilter
[LOG 02:04:55.610] FireCrew, .........c: UnityEngine.MeshRenderer
[LOG 02:04:55.611] FireCrew, .........c: UnityEngine.BoxCollider
[LOG 02:04:55.611] FireCrew, ------>label_stupidity has components:
[LOG 02:04:55.612] FireCrew, .........c: UnityEngine.Transform
[LOG 02:04:55.612] FireCrew, .........c: UnityEngine.MeshFilter
[LOG 02:04:55.613] FireCrew, .........c: UnityEngine.MeshRenderer
[LOG 02:04:55.614] FireCrew, .........c: SpriteText
[LOG 02:04:55.614] FireCrew, ------>name has components:
[LOG 02:04:55.615] FireCrew, .........c: UnityEngine.Transform
[LOG 02:04:55.616] FireCrew, .........c: UnityEngine.MeshFilter
[LOG 02:04:55.616] FireCrew, .........c: UnityEngine.MeshRenderer
[LOG 02:04:55.617] FireCrew, .........c: SpriteText
[LOG 02:04:55.617] FireCrew, ------>label_courage has components:
[LOG 02:04:55.618] FireCrew, .........c: UnityEngine.Transform
[LOG 02:04:55.619] FireCrew, .........c: UnityEngine.MeshFilter
[LOG 02:04:55.619] FireCrew, .........c: UnityEngine.MeshRenderer
[LOG 02:04:55.620] FireCrew, .........c: SpriteText
[LOG 02:04:55.620] FireCrew, ------>slider_courage has components:
[LOG 02:04:55.621] FireCrew, .........c: UnityEngine.Transform
[LOG 02:04:55.622] FireCrew, .........c: UISlider
[LOG 02:04:55.622] FireCrew, .........c: UnityEngine.MeshFilter
[LOG 02:04:55.623] FireCrew, .........c: UnityEngine.MeshRenderer
[LOG 02:04:55.623] FireCrew, .........c: UnityEngine.BoxCollider
[LOG 02:04:55.624] FireCrew, --------->slider_bg has components:
[LOG 02:04:55.625] FireCrew, ............c: UnityEngine.Transform
[LOG 02:04:55.625] FireCrew, ............c: BTButton
[LOG 02:04:55.626] FireCrew, ............c: UnityEngine.MeshFilter
[LOG 02:04:55.626] FireCrew, ............c: UnityEngine.MeshRenderer
[LOG 02:04:55.627] FireCrew, ............c: UnityEngine.BoxCollider
[LOG 02:04:55.628] FireCrew, --------->slider_fill has components:
[LOG 02:04:55.628] FireCrew, ............c: UnityEngine.Transform
[LOG 02:04:55.629] FireCrew, ............c: BTButton
[LOG 02:04:55.630] FireCrew, ............c: UnityEngine.MeshFilter
[LOG 02:04:55.630] FireCrew, ............c: UnityEngine.MeshRenderer
[LOG 02:04:55.631] FireCrew, ............c: UnityEngine.BoxCollider
[LOG 02:04:55.632] FireCrew, --------->slider_courage - Knob has components:
[LOG 02:04:55.632] FireCrew, ............c: UnityEngine.Transform
[LOG 02:04:55.633] FireCrew, ............c: UIScrollKnob
[LOG 02:04:55.633] FireCrew, ............c: UnityEngine.MeshFilter
[LOG 02:04:55.634] FireCrew, ............c: UnityEngine.MeshRenderer
[LOG 02:04:55.635] FireCrew, ............c: UnityEngine.BoxCollider
[LOG 02:04:55.635] FireCrew, --------->slider_courage - Empty Bar has components:
[LOG 02:04:55.636] FireCrew, ............c: UnityEngine.Transform
[LOG 02:04:55.637] FireCrew, ............c: AutoSprite
[LOG 02:04:55.637] FireCrew, ............c: UnityEngine.MeshFilter
[LOG 02:04:55.638] FireCrew, ............c: UnityEngine.MeshRenderer
[LOG 02:04:55.638] FireCrew, ------>slider_stupidity has components:
[LOG 02:04:55.639] FireCrew, .........c: UnityEngine.Transform
[LOG 02:04:55.640] FireCrew, .........c: UISlider
[LOG 02:04:55.640] FireCrew, .........c: UnityEngine.MeshFilter
[LOG 02:04:55.641] FireCrew, .........c: UnityEngine.MeshRenderer
[LOG 02:04:55.641] FireCrew, .........c: UnityEngine.BoxCollider
[LOG 02:04:55.642] FireCrew, --------->slider_bg has components:
[LOG 02:04:55.643] FireCrew, ............c: UnityEngine.Transform
[LOG 02:04:55.643] FireCrew, ............c: BTButton
[LOG 02:04:55.644] FireCrew, ............c: UnityEngine.MeshFilter
[LOG 02:04:55.645] FireCrew, ............c: UnityEngine.MeshRenderer
[LOG 02:04:55.645] FireCrew, ............c: UnityEngine.BoxCollider
[LOG 02:04:55.646] FireCrew, --------->slider_fill has components:
[LOG 02:04:55.646] FireCrew, ............c: UnityEngine.Transform
[LOG 02:04:55.647] FireCrew, ............c: BTButton
[LOG 02:04:55.648] FireCrew, ............c: UnityEngine.MeshFilter
[LOG 02:04:55.648] FireCrew, ............c: UnityEngine.MeshRenderer
[LOG 02:04:55.649] FireCrew, ............c: UnityEngine.BoxCollider
[LOG 02:04:55.649] FireCrew, --------->slider_stupidity - Knob has components:
[LOG 02:04:55.650] FireCrew, ............c: UnityEngine.Transform
[LOG 02:04:55.651] FireCrew, ............c: UIScrollKnob
[LOG 02:04:55.651] FireCrew, ............c: UnityEngine.MeshFilter
[LOG 02:04:55.652] FireCrew, ............c: UnityEngine.MeshRenderer
[LOG 02:04:55.653] FireCrew, ............c: UnityEngine.BoxCollider
[LOG 02:04:55.653] FireCrew, --------->slider_stupidity - Empty Bar has components:
[LOG 02:04:55.654] FireCrew, ............c: UnityEngine.Transform
[LOG 02:04:55.655] FireCrew, ............c: AutoSprite
[LOG 02:04:55.655] FireCrew, ............c: UnityEngine.MeshFilter
[LOG 02:04:55.656] FireCrew, ............c: UnityEngine.MeshRenderer
[LOG 02:04:55.657] FireCrew, ------>label has components:
[LOG 02:04:55.657] FireCrew, .........c: UnityEngine.Transform
[LOG 02:04:55.658] FireCrew, .........c: UnityEngine.MeshFilter
[LOG 02:04:55.658] FireCrew, .........c: UnityEngine.MeshRenderer
[LOG 02:04:55.659] FireCrew, .........c: SpriteText
[LOG 02:04:55.660] FireCrew, ENDLISTCONTAINER=====================
*/